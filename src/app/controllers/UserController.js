const githubAPI = require('../services/github.js')

class UserController {
  async index (req, res) {
    try {


      res.send({ message: 'ok' })
    } catch (error) {
      res.status(error.response.status).send(error.response.data.message)
    }
  }

}

module.exports = new UserController()

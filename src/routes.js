const express = require('express')
const routes = express.Router()

// controllers
const controllers = require('./app/controllers')

routes.get('/users', controllers.UserController.index)

module.exports = routes
